﻿using System;

namespace SmartGarage.CustomExceptions
{   //TODO: what is this custom exception for and how can it be used
    public class UserDoesNotExist : Exception
    {
        public UserDoesNotExist()
        {

        }

        public UserDoesNotExist(int id)
            : base(String.Format("User with id {0} does not exist", id))
        {

        }
    }
}