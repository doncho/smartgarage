﻿using System;

namespace SmartGarage.CustomExceptions
{   //TODO: what is this custom exception for and how can it be used
    public class InvalidVisit : Exception
    {
        public InvalidVisit()
        {

        }

        public InvalidVisit(string message)
            : base(String.Format("Invalid input"))
        {

        }
    }
}
