﻿namespace SmartGarage.CustomExceptions.CustomMessages
{
    //TODO: handle error message formatting
    public class ErrorMessages
    {
        public static string InValidPlateNumberMessage { get => "Invalid plate number, please re-enter plate in the following format: NN-DDDD-NN"; }

    }
}
