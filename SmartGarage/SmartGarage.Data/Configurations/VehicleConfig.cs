﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SmartGarage.Data.Models;

namespace SmartGarage.Data.Configurations
{
    internal class VehicleConfig : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            var vehicles = new Vehicle[]
            {
               new Vehicle{Id = 1,ModelId = 1, CustomerId = 2,PlateNumber="PB1824CM", IdentityNumber = "W6654654846465464",Type = (Models.Enums.VehicleTypes)1},
               new Vehicle{Id = 2,ModelId = 2, CustomerId = 3,PlateNumber="CA7774K", IdentityNumber = "W6654654846SMJRT4",Type = (Models.Enums.VehicleTypes)2},
               new Vehicle{Id = 3,ModelId = 3, CustomerId = 3,PlateNumber="PB7000KM", IdentityNumber = "W6654654846SLPQ64",Type = (Models.Enums.VehicleTypes)5},
               new Vehicle{Id = 4,ModelId = 4, CustomerId = 4,PlateNumber="PB7274CM", IdentityNumber = "W6654654846SKOP64",Type = (Models.Enums.VehicleTypes)4},
               new Vehicle{Id = 5,ModelId = 2, CustomerId = 5,PlateNumber="CO1234KX", IdentityNumber = "W66546DLOP6465464",Type = (Models.Enums.VehicleTypes)1},
               new Vehicle{Id = 6,ModelId = 3, CustomerId = 5,PlateNumber="A4526OP", IdentityNumber = "W6654654ASGH65464",Type = (Models.Enums.VehicleTypes)2},
               new Vehicle{Id = 7,ModelId = 1, CustomerId = 6,PlateNumber="CA0294KO", IdentityNumber = "W665NFHS846465464",Type = (Models.Enums.VehicleTypes)1},
               new Vehicle{Id = 8,ModelId = 4, CustomerId = 7,PlateNumber="TH4375AK", IdentityNumber = "W665NOPS846465464",Type = (Models.Enums.VehicleTypes)3},
               new Vehicle{Id = 9,ModelId = 2, CustomerId = 8,PlateNumber="B8203FS", IdentityNumber = "W66546OPSN6465464",Type = (Models.Enums.VehicleTypes)1},
               new Vehicle{Id = 10,ModelId = 5, CustomerId = 3,PlateNumber="CA5555BA", IdentityNumber = "W6654654ASDSA5464",Type = (Models.Enums.VehicleTypes)2},
               new Vehicle{Id = 11,ModelId = 6, CustomerId = 6,PlateNumber="PB7834PH", IdentityNumber = "W665465467DHA5464",Type = (Models.Enums.VehicleTypes)1},
               new Vehicle{Id = 12,ModelId = 7, CustomerId = 7,PlateNumber="CO4375AP", IdentityNumber = "W665NOPS8DSA65464",Type = (Models.Enums.VehicleTypes)3},
               new Vehicle{Id = 13,ModelId = 8, CustomerId = 8,PlateNumber="CB8203HM", IdentityNumber = "W66546OPSGLP65464",Type = (Models.Enums.VehicleTypes)1},
               new Vehicle{Id = 14,ModelId = 9, CustomerId = 5,PlateNumber="CA4526OK", IdentityNumber = "W6654654ARH865464",Type = (Models.Enums.VehicleTypes)2},
               new Vehicle{Id = 15,ModelId = 10, CustomerId = 4,PlateNumber="CA0294KO", IdentityNumber = "W665NFHJK76465464",Type = (Models.Enums.VehicleTypes)1},
               new Vehicle{Id = 16,ModelId = 11, CustomerId = 2,PlateNumber="TH4375AP", IdentityNumber = "W665NOP89IK465464",Type = (Models.Enums.VehicleTypes)3},
               new Vehicle{Id = 17,ModelId = 12, CustomerId = 8,PlateNumber="CA8203TE", IdentityNumber = "W66546RJK09465464",Type = (Models.Enums.VehicleTypes)1}
            };
            builder.Property<bool>("IsDeleted");
            builder.HasQueryFilter(m => EF.Property<bool>(m, "IsDeleted") == false);


            builder.HasData(vehicles);
        }
    }
}