﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SmartGarage.Data.Models;

namespace SmartGarage.Data.Configurations
{
    internal class ManufacturerConfig : IEntityTypeConfiguration<Manufacturer>
    {
        public void Configure(EntityTypeBuilder<Manufacturer> builder)
        {
            var manufacturers = new Manufacturer[]
            {
                new Manufacturer {Id = 1, Name = "Ford"},
                new Manufacturer {Id = 2, Name = "Opel"},
                new Manufacturer {Id = 3, Name = "Skoda"},
                new Manufacturer {Id = 4, Name = "Fiat"},
                new Manufacturer {Id = 5, Name = "Toyota"},
                new Manufacturer {Id = 6, Name = "Honda"},
                new Manufacturer {Id = 7, Name = "Lexus"},
                new Manufacturer {Id = 8, Name = "Jeep"},
                new Manufacturer {Id = 9, Name = "Chevrolet"},
                new Manufacturer {Id = 10, Name = "BMW"},
                new Manufacturer {Id = 11, Name = "Subaru"},
                new Manufacturer {Id = 12, Name = "Mercedes-Benz"}
            };
            builder.HasIndex(m => m.Name).IsUnique(true);
            builder.HasData(manufacturers);
        }
    }
}