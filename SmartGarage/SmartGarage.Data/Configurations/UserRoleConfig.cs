﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SmartGarage.Data.Configurations
{
    public class UserRoleConfig : IEntityTypeConfiguration<IdentityUserRole<int>>
    {
        public void Configure(EntityTypeBuilder<IdentityUserRole<int>> builder)
        {
            var roles = new IdentityUserRole<int>[]
            {
                new IdentityUserRole<int> { RoleId = 1, UserId = 1 },
                new IdentityUserRole<int> { RoleId = 2 , UserId = 2 },
                new IdentityUserRole<int> { RoleId = 2, UserId = 3 },
                new IdentityUserRole<int> { RoleId = 2 , UserId = 4 },
                new IdentityUserRole<int> { RoleId = 2, UserId = 5 },
                new IdentityUserRole<int> { RoleId = 2 , UserId = 6 },
                new IdentityUserRole<int> { RoleId = 2, UserId = 7 },
                new IdentityUserRole<int> { RoleId = 2 , UserId = 8 }
            };

            builder.HasData(roles);
        }
    }
}