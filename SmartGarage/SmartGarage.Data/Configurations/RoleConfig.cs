﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace SmartGarage.Data.Configurations
{
    public class RoleConfig : IEntityTypeConfiguration<IdentityRole<int>>
    {
        public void Configure(EntityTypeBuilder<IdentityRole<int>> builder)
        {
            var roles = new IdentityRole<int>[]
            {
                new IdentityRole<int> { Id = 1, Name = "Employee", NormalizedName = "EMPLOYEE" },
                new IdentityRole<int> { Id = 2, Name = "Customer", NormalizedName = "CUSTOMER" }
            };

            builder.HasData(roles);
        }
    }
}