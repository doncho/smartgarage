﻿using SmartGarage.Data.CustomAttributes;

namespace SmartGarage.Data.Models.Enums
{
    public enum Status
    {
        Ready,
        [StatusDescription("In-Progress")]
        [StatusDescription("InProgress")]
        InProgress,
        Waiting
    }
}