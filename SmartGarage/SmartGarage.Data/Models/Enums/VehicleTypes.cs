﻿namespace SmartGarage.Data.Models.Enums
{
    public enum VehicleTypes
    {
        Micro,
        Sedan,
        CUV,
        SUV,
        Hatchback,
        Roadster,
        Pickup,
        Van,
        Coupe,
        Supercar,
        Campervan,
        Minitruck,
        Cabriolet,
        Minivan,
        Truck,
        BigTruck
    }
}