﻿using SmartGarage.Data.Models.Enums;
using System.Collections.Generic;

namespace SmartGarage.Data.Models
{
    public class Vehicle
    {
        public int Id { get; set; }
        public int ModelId { get; set; }
        public VehicleModel Model { get; set; }
        public int CustomerId { get; set; }
        public User Customer { get; set; }
        public ICollection<Visit> Visits { get; set; }
        public string PlateNumber { get; set; }
        public VehicleTypes Type { get; set; }
        public string IdentityNumber { get; set; }
        public bool IsDeleted { get; set; }
    }
}