﻿using SmartGarage.Data.Models.Enums;
using System;
using System.Collections.Generic;

namespace SmartGarage.Data.Models
{
    public class Visit
    {
        public int Id { get; set; }
        public int VehicleId { get; set; }
        public Vehicle Vehicle { get; set; }
        public DateTime DateIn { get; set; }
        public DateTime? DateOut { get; set; }
        public Status Status { get; set; }
        public ICollection<Service> Services { get; set; }
        public bool IsDeleted { get; set; }

        //(optional) list parts
    }
}