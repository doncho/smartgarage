﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace SmartGarage.Data.CustomAttributes
{
    //TODO: this custom exception needs to be correctly implemented
    public class ValidPlateNumberAttribute : ValidationAttribute
    {

        public ValidPlateNumberAttribute(string plateNumber)
        {
            string input = (string)plateNumber;

            if (!Regex.Match(input, @"[A-Z]{1-2}\d{4}[A-Z]{2}").Success)
            {
                throw new Exception();
            }
            else
            {
                throw new ArgumentNullException();
            }
        }
    }
}
