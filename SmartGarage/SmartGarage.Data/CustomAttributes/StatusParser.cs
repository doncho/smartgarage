﻿using SmartGarage.CustomExceptions;
using SmartGarage.Data.Models.Enums;
using System.Collections.Generic;
using System.Reflection;

namespace SmartGarage.Data.CustomAttributes
{
    public static class StatusParser
    {
        private static readonly Dictionary<string, Status> stringToStatusMappings = new Dictionary<string, Status>();
        private static readonly Dictionary<Status, string> statusToStringMappings = new Dictionary<Status, string>();

        private static readonly string joinedStatuses;

        static StatusParser()
        {
            var fieldInfos = typeof(Status).GetFields(BindingFlags.Public | BindingFlags.Static);

            foreach (var fieldInfo in fieldInfos)
            {
                var genre = (Status)fieldInfo.GetValue(null);

                var enumDescriptionAttributes = (StatusDescriptionAttribute[])fieldInfo
                    .GetCustomAttributes(typeof(StatusDescriptionAttribute), false);

                if (enumDescriptionAttributes.Length == 0)
                {
                    stringToStatusMappings.Add(fieldInfo.Name.ToLower(), genre);
                    statusToStringMappings.Add(genre, fieldInfo.Name);
                }
                else
                {
                    statusToStringMappings.Add(genre, enumDescriptionAttributes[0].Description);

                    foreach (var enumDescriptionAttribute in enumDescriptionAttributes)
                    {
                        stringToStatusMappings.Add(enumDescriptionAttribute.Description.ToLower(), genre);
                    }
                }
            }

            joinedStatuses = string.Join(",", GetGenreValues());
        }

        public static Status Parse(string genreAsString)
        {
            if (string.IsNullOrEmpty(genreAsString))
            {
                throw new InvalidStatusException($"The string can not be null or empty. Valid values are: {joinedStatuses}");
            }

            var genreAsStringLowered = genreAsString.ToLower();
            if (!stringToStatusMappings.ContainsKey(genreAsStringLowered))
            {
                throw new InvalidStatusException($"The string: {genreAsString} is not a valid Status. Valid values are: {joinedStatuses}");
            }

            return stringToStatusMappings[genreAsStringLowered];
        }

        public static string ToString(Status status)
        {
            Validate(status);
            return statusToStringMappings[status];
        }

        public static void Validate(Status status)
        {
            if (!statusToStringMappings.ContainsKey(status))
            {
                throw new InvalidStatusException($"The Status: {status} is not a valid Status. Valid values are: {joinedStatuses}");
            }
        }

        public static IEnumerable<string> GetGenreValues()
        {
            foreach (var kvp in statusToStringMappings)
            {
                yield return kvp.Value;
            }
        }
    }
}
