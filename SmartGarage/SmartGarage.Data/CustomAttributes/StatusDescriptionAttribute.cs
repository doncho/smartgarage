﻿using System;

namespace SmartGarage.Data.CustomAttributes
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = false)]
    internal sealed class StatusDescriptionAttribute : Attribute
    {
        public string Description { get; }
        public StatusDescriptionAttribute(string description)
        {
            Description = description;
        }
    }
}
