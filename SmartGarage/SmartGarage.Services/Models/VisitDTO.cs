﻿using Newtonsoft.Json;
using SmartGarage.Data.Models;
using SmartGarage.Services.Extensions.CurrencyExtensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;


namespace SmartGarage.Services.Models
{
    public class VisitDTO
    {
        public VisitDTO()
        {
        }

        public VisitDTO(Visit model, string currency)
        {
            if (currency == null)
            {
                currency = "EUR";
            }

            this.Id = model.Id;
            this.VehicleId = model.VehicleId;
            this.PlateNumber = model.Vehicle.PlateNumber;
            this.VehicleModel = model.Vehicle.Model.Name;
            this.VehicleMake = model.Vehicle.Model.Manufacturer.Name;
            this.CustomerId = model.Vehicle.CustomerId;
            this.CustomerName = model.Vehicle.Customer.FirstName + " " + model.Vehicle.Customer.LastName;
            this.DateIn = model.DateIn.ToString("dd/MM/yyyy");

            if (model.DateOut == null)
            {
                this.DateOut = "No out date assigned yet";
            }
            else
            {
                this.DateOut = model.DateOut.Value.ToString("dd/MM/yyyy");
            }

            this.Status = (int)(model.Status);
            this.StatusName = model.Status.ToString();
            DateTime date;

            if (model.DateOut == null)
            {
                date = DateTime.Now;
            }
            else
            {
                date = model.DateOut.Value;
            }

            var exchange = CurrencyExchange.ExchangeCurrency(date, currency);

            this.TotalPrice = Math.Round(model.Services.Select(s => s.Price).Sum() * exchange.Result, 2);

            this.Services = model.Services.Select(s => new ServiceDTO(s, exchange.Result, date)).ToList();


        }

        [JsonIgnore]
        public int Id { get; set; }
        [JsonIgnore]
        [Display(Name = "Vehicle")]
        public int VehicleId { get; set; }
        [RegularExpression(@"[A-Z]{1-2}\d{4}[A-Z]{2}", ErrorMessage = "Invalid plate number")]
        public string PlateNumber { get; set; }

        [JsonIgnore]
        [Display(Name = "Customer")]
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string DateIn { get; set; }
        public string DateOut { get; set; }
        [JsonIgnore]
        public int Status { get; set; }
        public string VehicleModel { get; set; }
        public string VehicleMake { get; set; }
        public string StatusName { get; set; }
        public List<ServiceDTO> Services { get; set; }
        public decimal TotalPrice { get; set; }
        public string Currency { get; set; }
        public bool ToSend { get; set; }

    }
}