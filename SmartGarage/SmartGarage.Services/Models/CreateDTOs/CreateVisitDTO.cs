﻿using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Services.Models.CreateDTOs
{
    public class CreateVisitDTO
    {
        [Required]
        [Display(Name = "Vehicle")]
        public int VehicleId { get; set; }

        //TODO: do we need customer Id when a vehicle is linked to a customer
        //[Required]
        //[Display(Name = "Customer")]
        //public int CustomerId { get; set; }
    }
}
