﻿using Newtonsoft.Json;
using SmartGarage.Data.Models;
using SmartGarage.Data.Models.Enums;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Services.Models
{
    public class VehicleModelDTO
    {
        public VehicleModelDTO()
        {
        }

        public VehicleModelDTO(VehicleModel model)
        {
            this.Id = model.Id;
            this.Name = model.Name;
            this.ManufacturerId = model.ManufacturerId;
            this.Manufacturer = model.Manufacturer.Name;
        }
        [JsonIgnore]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
        [JsonIgnore]
        [Required]
        public int ManufacturerId { get; set; }

        public string Manufacturer { get; set; }
        public IEnumerable<Manufacturer> Manufacturers  { get; set; }

    }
}