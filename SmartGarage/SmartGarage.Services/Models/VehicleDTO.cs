﻿using Newtonsoft.Json;
using SmartGarage.Data.Models;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Services.Models
{
    public class VehicleDTO
    {
        public VehicleDTO()
        {
        }

        public VehicleDTO(Vehicle model)
        {
            this.Id = model.Id;
            this.ManufacturerId = model.Model.ManufacturerId;
            this.Manufacturer = model.Model.Manufacturer.Name;
            this.ModelId = model.ModelId;
            this.Model = model.Model.Name;
            this.CustomerId = model.CustomerId;
            //this.Visits = model.Visits;
            this.PlateNumber = model.PlateNumber;
            this.Type = (int)model.Type;
            this.TypeString = model.Type.ToString();
            this.IdentityNumber = model.IdentityNumber;
            this.CustomerFullName = model.Customer.FirstName + " " + model.Customer.LastName;
            this.Name = model.Model.Manufacturer.Name + " " + model.Model.Name + " " + model.PlateNumber;
        }

        [JsonIgnore]
        public int Id { get; set; }

        [JsonIgnore]
        [Display(Name = "Manufacturer")]
        public int ManufacturerId { get; set; }
        public string Manufacturer { get; set; }
        [JsonIgnore]
        [Display(Name = "Model")]
        public int ModelId { get; set; }

        public string Model { get; set; }
        [JsonIgnore]
        [Display(Name = "Customer")]
        public int CustomerId { get; set; }

        //public IEnumerable<Visit> Visits { get; set; }
        [Display(Name = "Plate Number")]
        [RegularExpression(@"[A-Z]{1-2}\d{4}[A-Z]{2}", ErrorMessage = "Invalid plate number")]
        public string PlateNumber { get; set; }
        [JsonIgnore]
        public int Type { get; set; }

        public string TypeString { get; set; }

        [Required]
        [Display(Name = "Identity Number")]
        [RegularExpression(@"/^([1-9]|[A-Z]){17}$/", ErrorMessage = "Invalid vehicle identity number")]
        public string IdentityNumber { get; set; }

        [Display(Name = "Customer Name")]
        [JsonIgnore]
        public string CustomerFullName { get; set; }
        [JsonIgnore]
        public string Name { get; set; }
    }
}