﻿using SmartGarage.Data.Models;

namespace SmartGarage.Services.Models
{
    public class UserAuthDTO
    {
        public UserAuthDTO(User user)
        {
            this.Username = user.UserName;
        }

        public string Username { get; set; }
        public string Token { get; set; }
    }
}