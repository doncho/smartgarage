﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Services.Models.UpdateDTOs
{
    public class UpdateVisitDTO
    {
        [Required]
        [Range(0, 2)]
        public int Status { get; set; }
    }
}
