﻿using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Services.Models.UpdateDTOs
{
    public class UpdateUserDTO
    {
        [EmailAddress]
        public string Email { get; set; }

        [MinLength(2), MaxLength(20)]
        public string FirstName { get; set; }

        [MinLength(2), MaxLength(20)]
        public string LastName { get; set; }

        [RegularExpression(@"[0]{1}\d{9}", ErrorMessage = "Invalid phone number")]
        public string Phone { get; set; }
    }
}
