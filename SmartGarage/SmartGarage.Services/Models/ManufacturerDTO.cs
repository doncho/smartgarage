﻿using Newtonsoft.Json;
using SmartGarage.Data.Models;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Services.Models
{
    public class ManufacturerDTO
    {
        public ManufacturerDTO()
        {
        }

        public ManufacturerDTO(Manufacturer model)
        {
            this.Id = model.Id;
            this.Name = model.Name;
        }
        [JsonIgnore]
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}