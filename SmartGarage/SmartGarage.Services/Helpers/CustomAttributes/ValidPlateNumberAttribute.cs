﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace SmartGarage.Services.Helpers.CustomAttributes
{
    //TODO: this custom exception needs to be correctly implemented and probably validate the DTO, look at the same class in Data. Check which base class you need to override
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter, AllowMultiple = true)]
    public class ValidPlateNumberAttribute : ValidationAttribute
    {
        public ValidPlateNumberAttribute(string plateNumber)
        {
            PlateNumber = plateNumber;
        }

        public string PlateNumber { get; }

        public static string PlateErrorMessage() => "Invalid plate number";

        protected override ValidationResult IsValid(object plateNumber, ValidationContext validationContext)
        {
            string plateNumberToString = (string)plateNumber;

            if (!Regex.Match(plateNumberToString, @"[A-Z]{1-2}\d{4}[A-Z]{2}").Success)
            {
                return new ValidationResult(PlateErrorMessage());
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }
}
