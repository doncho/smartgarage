﻿using SmartGarage.Services.Helpers;
using SmartGarage.Services.Models;
using SmartGarage.Services.Models.CreateDTOs;
using SmartGarage.Services.Models.UpdateDTOs;
using SmartGarage.Services.QueryObject;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services.Interfaces
{
    public interface IVehicleService
    {
        public Task<VehicleDTO> GetAsync(int? id);

        public Task<Pager<VehicleDTO>> GetAllAsync(string customerName, PagerQueryObject pagerQuery, int customerId = 0);

        public Task<IEnumerable<VehicleDTO>> GetAllAsync();

        public Task<bool> CreateAsync(CreateVehicleDTO model);

        public Task<bool> UpdateAsync(UpdateVehicleDTO model, int id);

        public Task<bool> DeleteAsync(int id);

        //Task<IEnumerable<VehicleDTO>> GetAllVehicleIdsAsync();
    }
}