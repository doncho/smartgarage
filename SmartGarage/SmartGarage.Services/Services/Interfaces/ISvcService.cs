﻿using SmartGarage.Services.Helpers;
using SmartGarage.Services.Models;
using SmartGarage.Services.QueryObject;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services.Interfaces
{
    public interface ISvcService
    {
        public Task<bool> CreateAsync(ServiceDTO model);

        public Task<ServiceDTO> GetAsync(int id);

        public Task<Pager<ServiceDTO>> GetAllAsync(FilterServiceQuery serviceQuery, PagerQueryObject pagerQuery);

        public Task<IEnumerable<ServiceDTO>> GetAllAsync();

        public Task<bool> UpdateAsync(int id, ServiceDTO model);

        public Task<bool> DeleteAsync(int id);
    }
}