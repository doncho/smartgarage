﻿using SmartGarage.Services.Helpers;
using SmartGarage.Services.Models;
using SmartGarage.Services.Models.CreateDTOs;
using SmartGarage.Services.Models.UpdateDTOs;
using SmartGarage.Services.QueryObject;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services.Interfaces
{
    public interface IVisitService
    {
        public Task<VisitDTO> GetAsync(int id, string currency = "EUR");
        public Task<Pager<VisitDTO>> GetAllAsync(FilterVisitQuery visitQuery, PagerQueryObject pagerQuery, string currency, string isCheckedOut = "All", int customerId = 0);
        public Task<bool> DeleteServiceFromVisit(int visitId, int serviceId);
        public Task<IEnumerable<VisitDTO>> GetAllAsync(int id = 0);
        public Task<int> Create(CreateVisitDTO model);
        public Task<bool> ChangeStatus(int id, int status);
        public Task<bool> CreateAsync(CreateVisitDTO model);

        public Task<bool> UpdateAsync(int id, UpdateVisitDTO model);

        public Task<bool> DeleteAsync(int id);
        public Task<bool> AddServiceToVisit(int visitId, int serviceId);
        public Task<bool> CheckOutVisit(int id);
    }
}