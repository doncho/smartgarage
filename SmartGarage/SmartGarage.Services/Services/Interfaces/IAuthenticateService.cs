﻿using SmartGarage.Services.Models;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services.Interfaces
{
    public interface IAuthenticateService
    {
        public Task<UserAuthDTO> AuthenticateAsync(LoginDTO loginDTO);
    }
}