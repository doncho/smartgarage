﻿using SmartGarage.Services.Helpers;
using SmartGarage.Services.Models;
using SmartGarage.Services.QueryObject;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services.Interfaces
{
    public interface IVehicleModelService
    {
        public Task<Pager<VehicleModelDTO>> GetAllAsync(string manufacturer, PagerQueryObject pagerQuery);

        public Task<IEnumerable<VehicleModelDTO>> GetAllAsync();
        public Task<VehicleModelDTO> GetAsync(string name);

        public Task<VehicleModelDTO> GetAsync(int id);

        public Task<bool> CreateAsync(VehicleModelDTO model);

        public Task<bool> UpdateAsync(VehicleModelDTO model, int id);

        public Task<bool> DeleteAsync(int id);
    }
}