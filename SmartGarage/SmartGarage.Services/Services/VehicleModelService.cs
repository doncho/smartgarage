﻿using Microsoft.EntityFrameworkCore;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Helpers;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    public class VehicleModelService : IVehicleModelService
    {
        private readonly SmartGarageContext context;
        private readonly IModelMapper modelMapper;

        public VehicleModelService(SmartGarageContext context, IModelMapper modelMapper)
        {
            this.context = context;
            this.modelMapper = modelMapper;
        }

        public async Task<Pager<VehicleModelDTO>> GetAllAsync(string manufacturer, PagerQueryObject pagerQuery)
        {
            var skip = (pagerQuery.PageNumber - 1) * pagerQuery.ItemsOnPage;
            IQueryable<VehicleModel> vehicleModels = context.VehicleModels.Include(vm => vm.Manufacturer);

            if(manufacturer != null)
            {
                vehicleModels = vehicleModels.Where(vm => vm.Manufacturer.Name == manufacturer);
            }

            var vehicleModelsDTOs = await vehicleModels.Skip(skip)
                .Take(pagerQuery.ItemsOnPage)
                .Select(vm => new VehicleModelDTO(vm))
                .ToListAsync();

            Pager<VehicleModelDTO> result = new Pager<VehicleModelDTO>(vehicleModelsDTOs, pagerQuery, vehicleModels.Count());

            return result;
        }

        public async Task<IEnumerable<VehicleModelDTO>> GetAllAsync()
        {
            return await context.VehicleModels.Include(vm => vm.Manufacturer).Select(vm => new VehicleModelDTO(vm)).ToListAsync();
        }
        public async Task<VehicleModelDTO> GetAsync(string name)
        {
            var vehicleModel = await context.VehicleModels.Include(vm => vm.Manufacturer).FirstOrDefaultAsync(vm => vm.Name == name);
            if (vehicleModel == null)
            {
                return null;
            }
            return new VehicleModelDTO(vehicleModel);
        }
        public async Task<VehicleModelDTO> GetAsync(int id)
        {
            var vehicleModel = await context.VehicleModels.Include(vm => vm.Manufacturer).FirstOrDefaultAsync(vm => vm.Id == id);
            if (vehicleModel == null)
            {
                return null;
            }
            return new VehicleModelDTO(vehicleModel);
        }

        public async Task<bool> CreateAsync(VehicleModelDTO model)
        {
            var vehicleModelToCreate = modelMapper.ToVehicleModel(model);
            if (vehicleModelToCreate == null)
            {
                return false;
            }
            await context.VehicleModels.AddAsync(vehicleModelToCreate);
            await context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdateAsync(VehicleModelDTO model, int id)
        {
            var vehicleModel = await context.VehicleModels.FirstOrDefaultAsync(m => m.Id == id);

            if (vehicleModel == null || model == null)
            {
                return false;
            }

            vehicleModel.Name = model.Name ?? vehicleModel.Name;

            if (model.ManufacturerId != default)
            {
                vehicleModel.ManufacturerId = model.ManufacturerId;
            }

            context.VehicleModels.Update(vehicleModel);
            await context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var vehicleModel = await context.VehicleModels.FirstOrDefaultAsync(v => v.Id == id);
            if (vehicleModel == null)
            {
                return false;
            }
            context.VehicleModels.Remove(vehicleModel);
            await context.SaveChangesAsync();

            return true;
        }
    }
}