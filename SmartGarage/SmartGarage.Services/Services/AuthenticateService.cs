﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SmartGarage.Data;
using SmartGarage.Data.Models;
using SmartGarage.Services.Helpers;
using SmartGarage.Services.Models;
using SmartGarage.Services.Services.Interfaces;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Services.Services
{
    public class AuthenticateService : IAuthenticateService
    {
        private readonly SignInManager<User> signInManager;
        private readonly UserManager<User> userManager;
        private readonly IOptions<AppSettings> appSettings;
        private readonly SmartGarageContext context;

        public AuthenticateService(SignInManager<User> signInManager,
            UserManager<User> userManager,
            IOptions<AppSettings> appSettings,
            SmartGarageContext smartGarageContext)
        {
            this.signInManager = signInManager;
            this.userManager = userManager;
            this.appSettings = appSettings;
            this.context = smartGarageContext;
        }

        public async Task<UserAuthDTO> AuthenticateAsync(LoginDTO loginDTO)
        {
            var user = await this.context.Users
            .SingleOrDefaultAsync(u => u.UserName == loginDTO.Username);

            // return null if user not found
            if (user != null)
            {
                var signInAttempt = await this.signInManager.CheckPasswordSignInAsync(user, loginDTO.Password, false);

                if (signInAttempt.Succeeded)
                {
                    var userRole = (await this.userManager.GetRolesAsync(user)).FirstOrDefault();

                    // authentication successful so generate jwt token
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var key = Encoding.ASCII.GetBytes(appSettings.Value.Secret);
                    var tokenDescriptor = new SecurityTokenDescriptor
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, user.Id.ToString()),
                        new Claim(ClaimTypes.Role, userRole)
                    }),
                        Expires = DateTime.UtcNow.AddDays(7),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                    };

                    var token = tokenHandler.CreateToken(tokenDescriptor);

                    UserAuthDTO result = new UserAuthDTO(user);

                    result.Token = tokenHandler.WriteToken(token);

                    return result;
                }
            }
            return null;
        }
    }
}