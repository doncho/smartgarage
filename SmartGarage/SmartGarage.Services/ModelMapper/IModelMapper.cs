﻿using SmartGarage.Data.Models;
using SmartGarage.Services.Models;
using SmartGarage.Services.Models.CreateDTOs;

namespace SmartGarage.Services.ModelMapper
{
    public interface IModelMapper
    {
        public Manufacturer ToManufacturer(ManufacturerDTO manufacturerDTO);
        public VehicleModel ToVehicleModelWithId(VehicleModelDTO vehicleModelDTO);

        public VehicleModel ToVehicleModel(VehicleModelDTO vehicleModelDTO);

        public Vehicle ToVehicle(CreateVehicleDTO vehicleDTO);

        public Visit ToVisit(CreateVisitDTO visitDTO);

        public Service ToService(ServiceDTO serviceDTO);

        public User ToCustomer(CreateUserDTO userDTO);
    }
}