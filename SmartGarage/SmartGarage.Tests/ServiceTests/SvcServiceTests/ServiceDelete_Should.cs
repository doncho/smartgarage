﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.SvcServiceTests
{
    [TestClass]
    public class ServiceDelete_Should
    {
        [TestMethod]
        public async Task ServiceDeleteCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("ServiceDeleteCorrectly");
            var serviceToDelete = Utils.GetServices().FirstOrDefault();
            var servicesCount = Utils.GetServices().Count();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new SvcService(actContext, modelMapper);
                var result = await sut.DeleteAsync(serviceToDelete.Id);

                //Assert
                Assert.IsTrue(result);
                Assert.AreEqual(servicesCount - 1, 2);
            }
        }
        [TestMethod]
        public async Task ServiceDeleteWrongID()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("ServiceDeleteWrongID");

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new SvcService(actContext, modelMapper);
                var result = await sut.DeleteAsync(0);

                //Assert
                Assert.IsFalse(result);

            }

        }
    }
}
