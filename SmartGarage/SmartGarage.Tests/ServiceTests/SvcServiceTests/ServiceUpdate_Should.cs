﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.SvcServiceTests
{
    [TestClass]
    public class ServiceUpdate_Should
    {
        [TestMethod]
        public async Task ServiceUpdateCorrectly()
        {
            //TODO: can be improved by validating the exact values of the model and the returned new service price
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("ServiceUpdateCorrectly");
            var service = Utils.GetServices().FirstOrDefault();
            var model = new ServiceDTO();
            model.Price = 123m;

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new SvcService(actContext, modelMapper);
                var result = await sut.UpdateAsync(1, model);

                //Assert
                Assert.IsTrue(result);
            }

        }

        [TestMethod]
        public async Task ServiceUpdateWrongID()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("ServiceUpdateWrongID");
            var model = new ServiceDTO();
            model.Name = "Test";


            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new SvcService(actContext, modelMapper);
                var result = await sut.UpdateAsync(0, model);

                //Assert
                Assert.IsFalse(result);
            }
        }
    }
}
