﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.SvcServiceTests
{
    [TestClass]
    public class ServiceGet_Should
    {
        [TestMethod]
        public async Task ServiceGetAllCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("ServiceGetAllCorrectly");
            var services = Utils.GetServices();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new SvcService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAsync();

                //Assert
                Assert.AreEqual(services.Count(), result.Count());
            }
        }

        [TestMethod]
        public async Task ServiceGetAllCorrectlyWithPager()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("ServiceGetAllCorrectlyWithPager");
            var services = Utils.GetServices();
            PagerQueryObject pagerQuery = new PagerQueryObject(1, services.Count() - 1);
            FilterServiceQuery serviceQuery = new FilterServiceQuery();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new SvcService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAsync(serviceQuery, pagerQuery);

                //Assert
                Assert.AreEqual(services.Count() - 1, result.Items.Count);
            }
        }

        [TestMethod]
        public async Task ServiceGetByIdCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("ServiceGetByIdCorrectly");
            var service = Utils.GetServices().FirstOrDefault();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new SvcService(actContext, modelMapperFake.Object);
                var result = await sut.GetAsync(service.Id);

                //Assert
                Assert.AreEqual(service.Id, result.Id);
            }
        }

        [TestMethod]
        public async Task ServiceGetByIdReturnNullForNotFound()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("ServiceGetByIdReturnNullForNotFound");

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new SvcService(actContext, modelMapperFake.Object);
                var result = await sut.GetAsync(0);

                //Assert
                Assert.IsNull(result);
            }
        }

        [TestMethod]
        public async Task ServiceGetFilterByNameCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("ServiceGetFilterByNameCorrectly");
            var visit = Utils.GetServices().Where(v => v.Name == "Clean");
            PagerQueryObject pagerQuery = new PagerQueryObject(1, visit.Count() - 1);
            FilterServiceQuery serviceQuery = new FilterServiceQuery();
            serviceQuery.Name = "Clean";

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new SvcService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAsync(serviceQuery, pagerQuery);

                //Assert
                Assert.AreEqual(visit.Count(), result.Count);
            }
        }

        [TestMethod]
        public async Task ServiceGetFilterByPriceCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("ServiceGetFilterByPriceCorrectly");
            PagerQueryObject pagerQuery = new PagerQueryObject(0, 0);
            FilterServiceQuery serviceQuery = new FilterServiceQuery();
            serviceQuery.PriceFrom = 10m;
            serviceQuery.PriceTo = 30m;
            int count = Utils.GetServices().Where(x => x.Price > 10m && x.Price < 40m).Count();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new SvcService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAsync(serviceQuery, pagerQuery);

                //Assert
                Assert.AreEqual(1, result.Count);
            }
        }

    }
}

