﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.SvcServiceTests
{
    [TestClass]
    public class ServiceCreate_Should
    {
        [TestMethod]
        public async Task ServiceModelCreateCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("ServiceModelCreateCorrectly");
            var serviceModelsCount = Utils.GetServices().Count();
            var model = new ServiceDTO();
            model.Name = "Checkup";
            model.Price = 40m;

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new SvcService(actContext, modelMapper);
                var result = await sut.CreateAsync(model);

                //Assert
                Assert.IsTrue(result);
                Assert.AreEqual(serviceModelsCount + 1, actContext.Services.Count());
            }
        }
    }
}
