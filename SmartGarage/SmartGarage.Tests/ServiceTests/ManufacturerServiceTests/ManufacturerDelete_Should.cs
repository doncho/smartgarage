﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.ManufacturerServiceTests
{
    [TestClass]
    public class ManufacturerDelete_Should
    {
        [TestMethod]
        public async Task ManufaturerDeleteCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("ManufaturerDeleteCorrectly");
            var manufaturerToDelete = Utils.GetManufacturers().FirstOrDefault();



            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new ManufacturerService(actContext, modelMapper);
                var result = await sut.DeleteAsync(manufaturerToDelete.Id);

                //Assert
                Assert.IsTrue(result);
                Assert.IsTrue(actContext.Manufacturers.FirstOrDefault(x => x.Id == manufaturerToDelete.Id).IsDeleted);

            }

        }
        [TestMethod]
        public async Task ManufaturerDeleteWrongID()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("ManufaturerDeleteWrongID");



            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new ManufacturerService(actContext, modelMapper);
                var result = await sut.DeleteAsync(0);

                //Assert
                Assert.IsFalse(result);

            }

        }
    }
}
