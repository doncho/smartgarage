﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.ManufacturerServiceTests
{
    [TestClass]
    public class ManufacturerCreate_Should
    {
        [TestMethod]
        public async Task ManufaturerCreateCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("ManufaturerCreateCorrectly");
            var manufaturersCount = Utils.GetManufacturers().Count();
            var model = new ManufacturerDTO();
            model.Name = "Test";


            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new ManufacturerService(actContext, modelMapper);
                var result = await sut.CreateAync(model);

                //Assert
                Assert.IsTrue(result);
                Assert.AreEqual(manufaturersCount + 1, actContext.Manufacturers.Count());

            }

        }
    }
}
