﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.ManufacturerServiceTests
{
    [TestClass]
    public class ManufacturerUpdate_Should
    {
        [TestMethod]
        public async Task ManufaturerUpdateCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("ManufaturerUpdateCorrectly");
            var model = new ManufacturerDTO();
            model.Name = "Test";


            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new ManufacturerService(actContext, modelMapper);
                var result = await sut.UpdateAsync(model, 1);

                //Assert
                Assert.IsTrue(result);
                Assert.AreEqual(model.Name, actContext.Manufacturers.FirstOrDefault(x => x.Id == 1).Name);

            }

        }
        [TestMethod]
        public async Task ManufaturerUpdateWrongID()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("ManufaturerUpdateWrongID");
            var model = new ManufacturerDTO();
            model.Name = "Test";


            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new ManufacturerService(actContext, modelMapper);
                var result = await sut.UpdateAsync(model, 0);

                //Assert
                Assert.IsFalse(result);

            }

        }

    }
}
