﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.VehicleModelTests
{
    [TestClass]
    public class VehicleModelDelete_Should
    {
        [TestMethod]
        public async Task VehicleModelDeleteCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("VehicleModelDeleteCorrectly");
            var vehicleModelToDelete = Utils.GetVehicleModels().FirstOrDefault();



            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VehicleModelService(actContext, modelMapper);
                var result = await sut.DeleteAsync(vehicleModelToDelete.Id);

                //Assert
                Assert.IsTrue(result);
                Assert.IsTrue(actContext.VehicleModels.IgnoreQueryFilters().FirstOrDefault(x => x.Id == vehicleModelToDelete.Id).IsDeleted);

            }

        }
        [TestMethod]
        public async Task VehicleModelDeleteWrongID()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("VehicleModelDeleteWrongID");



            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VehicleModelService(actContext, modelMapper);
                var result = await sut.DeleteAsync(0);

                //Assert
                Assert.IsFalse(result);

            }

        }
    }
}
