﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.VehicleModelTests
{
    [TestClass]
    public class VehicleModelCreate_Should
    {
        [TestMethod]
        public async Task VehicleModelCreateCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("VehicleModelCreateCorrectly");
            var vehicleModelsCount = Utils.GetVehicleModels().Count();
            var model = new VehicleModelDTO();
            model.Name = "Test";
            model.ManufacturerId = 1;


            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VehicleModelService(actContext, modelMapper);
                var result = await sut.CreateAsync(model);

                //Assert
                Assert.IsTrue(result);
                Assert.AreEqual(vehicleModelsCount + 1, actContext.VehicleModels.Count());

            }

        }
    }
}
