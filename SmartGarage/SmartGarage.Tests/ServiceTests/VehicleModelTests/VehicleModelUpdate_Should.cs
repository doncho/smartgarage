﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.VehicleModelTests
{
    [TestClass]
    public class VehicleModelUpdate_Should
    {
        [TestMethod]
        public async Task VehicleModelUpdateCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("VehicleModelUpdateCorrectly");
            var model = new VehicleModelDTO();
            model.Name = "Test";


            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VehicleModelService(actContext, modelMapper);
                var result = await sut.UpdateAsync(model, 1);

                //Assert
                Assert.IsTrue(result);
                Assert.AreEqual(model.Name, actContext.VehicleModels.FirstOrDefault(x => x.Id == 1).Name);

            }

        }
        [TestMethod]
        public async Task VehicleModelUpdateWrongID()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("VehicleModelUpdateWrongID");
            var model = new VehicleModelDTO();
            model.Name = "Test";


            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VehicleModelService(actContext, modelMapper);
                var result = await sut.UpdateAsync(model, 0);

                //Assert
                Assert.IsFalse(result);

            }

        }

    }
}
