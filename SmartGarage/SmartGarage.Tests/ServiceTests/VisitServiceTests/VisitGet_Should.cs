﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.VisitServiceTests
{
    [TestClass]
    public class VisitGet_Should
    {
        [TestMethod]
        public async Task VisitGetAllCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VisitGetAllCorrectly");
            var visits = Utils.GetVisits();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAsync();

                //Assert
                Assert.AreEqual(visits.Count(), result.Count());
            }
        }

        [TestMethod]
        public async Task VisitGetAllCorrectlyWithPager()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VisitGetAllCorrectlyWithPager");
            var visits = Utils.GetVisits();
            PagerQueryObject pagerQuery = new PagerQueryObject(1, visits.Count() - 1);

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new ManufacturerService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAync(pagerQuery);

                //Assert
                Assert.AreEqual(visits.Count() - 1, result.Items.Count);
            }
        }

        [TestMethod]
        public async Task VisitGetByIdCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VisitGetByIdCorrectly");
            var visit = Utils.GetVisits().FirstOrDefault();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapperFake.Object);
                var result = await sut.GetAsync(visit.Id, null);

                //Assert
                Assert.AreEqual(visit.Id, result.Id);
                Assert.AreEqual(visit.Status.ToString(), result.StatusName);
            }
        }

        [TestMethod]
        public async Task VisitGetByIdReturnNullForNotFound()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VisitGetByIdReturnNullForNotFound");

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapperFake.Object);
                var result = await sut.GetAsync(0, null);

                //Assert
                Assert.IsNull(result);
            }
        }

        [TestMethod]
        public async Task VisitGetFilterByStatusCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VisitGetFilterByStatusCorrectly");
            var visit = Utils.GetVisits().Where(v => v.Status == (Data.Models.Enums.Status)1);
            PagerQueryObject pagerQuery = new PagerQueryObject(1, visit.Count() - 1);
            FilterVisitQuery visitQuery = new FilterVisitQuery();
            visitQuery.Status = "In-progress";

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAsync(visitQuery, pagerQuery, null);

                //Assert
                Assert.AreEqual(visit.Count(), result.Count);
            }
        }

        [TestMethod]
        public async Task VisitGetFilterByFirstNameCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VisitGetFilterByFirstNameCorrectly");
            PagerQueryObject pagerQuery = new PagerQueryObject(0, 0);
            FilterVisitQuery visitQuery = new FilterVisitQuery();
            visitQuery.FirstName = "Georgi";

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAsync(visitQuery, pagerQuery, null);

                //Assert
                Assert.AreEqual(1, result.Count);
            }
        }

        [TestMethod]
        public async Task VisitGetFilterByLastNameCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VisitGetFilterByLastNameCorrectly");
            PagerQueryObject pagerQuery = new PagerQueryObject(0, 0);
            FilterVisitQuery visitQuery = new FilterVisitQuery();
            visitQuery.LastName = "Ivanov";

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAsync(visitQuery, pagerQuery, null);

                //Assert
                Assert.AreEqual(1, result.Count);
            }
        }

        [TestMethod]
        public async Task VisitGetFilterByPlateNumberCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VisitGetFilterByPlateNumberCorrectly");
            PagerQueryObject pagerQuery = new PagerQueryObject(0, 0);
            FilterVisitQuery visitQuery = new FilterVisitQuery();
            visitQuery.PlateNumber = "PB1824CM";

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAsync(visitQuery, pagerQuery, null);

                //Assert
                Assert.AreEqual(1, result.Count);
            }
        }

        [TestMethod]
        public async Task VisitGetFilterByIdentityNumberCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VisitGetFilterByIdentityNumberCorrectly");
            PagerQueryObject pagerQuery = new PagerQueryObject(0, 0);
            FilterVisitQuery visitQuery = new FilterVisitQuery();
            visitQuery.IdentityNumber = "W6654654846465464";

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAsync(visitQuery, pagerQuery, null);

                //Assert
                Assert.AreEqual(1, result.Count);
            }
        }

        [TestMethod]
        public async Task VisitGetFilterByDateRangeCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("VisitGetFilterByDateRangeCorrectly");
            PagerQueryObject pagerQuery = new PagerQueryObject(0, 0);
            FilterVisitQuery visitQuery = new FilterVisitQuery();
            visitQuery.DateFrom = DateTime.Now.AddDays(-2);
            visitQuery.DateTo = DateTime.Now.AddDays(2);

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllAsync(visitQuery, pagerQuery, null);

                //Assert
                Assert.AreEqual(1, result.Count);
            }
        }
    }
}
