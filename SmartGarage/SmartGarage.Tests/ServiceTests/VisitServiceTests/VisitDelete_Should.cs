﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.VisitServiceTests
{
    [TestClass]
    public class VisitDelete_Should
    {
        [TestMethod]
        public async Task VisitModelDeleteCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("VisitModelDeleteCorrectly");
            var visitToDelete = Utils.GetVisits().FirstOrDefault();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapper);
                var result = await sut.DeleteAsync(visitToDelete.Id);

                //Assert
                Assert.IsTrue(result);
                Assert.IsTrue(actContext.Visits.FirstOrDefault(x => x.Id == visitToDelete.Id).IsDeleted);
            }
        }
        [TestMethod]
        public async Task VisitModelDeleteWrongID()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("VisitModelDeleteWrongID");

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VisitService(actContext, modelMapper);
                var result = await sut.DeleteAsync(0);

                //Assert
                Assert.IsFalse(result);

            }

        }
    }
}
