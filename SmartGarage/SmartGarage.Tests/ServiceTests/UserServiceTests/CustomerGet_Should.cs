﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.UserServiceTests
{
    [TestClass]
    public class CustomerGet_Should
    {
        [TestMethod]
        public async Task CustomerGetAllCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("CustomerGetAllCorrectly");
            var customers = Utils.GetUserRoles().Where(r => r.RoleId == 2);

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new UserService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllCustomersAsync();

                //Assert
                Assert.AreEqual(customers.Count(), result.Count());
            }
        }

        [TestMethod]
        public async Task CustomerGetAllCorrectlyWithPager()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("CustomerGetAllCorrectlyWithPager");
            var customers = Utils.GetUserRoles().Where(r => r.RoleId == 2);
            PagerQueryObject pagerQuery = new PagerQueryObject(1, customers.Count() - 1);
            FilterUserQuery userQuery = new FilterUserQuery();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new UserService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllCustomersAsync(userQuery, pagerQuery);

                //Assert
                Assert.AreEqual(customers.Count() - 1, result.Items.Count);
            }
        }

        [TestMethod]
        public async Task CustomerGetByIdCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("CustomerGetByIdCorrectly");
            var customer = Utils.GetUserRoles().Where(u => u.RoleId == 2).FirstOrDefault();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new UserService(actContext, modelMapperFake.Object);
                var result = await sut.GetCustomerAsync(customer.UserId);

                //Assert
                Assert.AreEqual(customer.UserId, result.Id);
            }
        }

        [TestMethod]
        public async Task CustomerGetByIdReturnNullForNotFound()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("CustomerGetByIdReturnNullForNotFound");

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new UserService(actContext, modelMapperFake.Object);
                var result = await sut.GetCustomerAsync(0);

                //Assert
                Assert.IsNull(result);
            }
        }

        [TestMethod]
        public async Task CustomerGetFilterByNameCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("CustomerGetFilterByNameCorrectly");
            var customer = Utils.GetUserRoles().Where(u => u.RoleId == 2).FirstOrDefault();
            PagerQueryObject pagerQuery = new PagerQueryObject(1, 1);
            FilterUserQuery userQuery = new FilterUserQuery();

            userQuery.Name = "Georgi Ivanov";

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new UserService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllCustomersAsync(userQuery, pagerQuery);

                //Assert
                Assert.AreEqual(1, result.Count);
            }
        }

        [TestMethod]
        public async Task CustomerGetFilterByEmailCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("CustomerGetFilterByEmailCorrectly");
            var customer = Utils.GetUserRoles().Where(u => u.RoleId == 2).FirstOrDefault();
            PagerQueryObject pagerQuery = new PagerQueryObject(1, 1);
            FilterUserQuery userQuery = new FilterUserQuery();

            userQuery.Email = "georgiivanov@gmail.com";

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new UserService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllCustomersAsync(userQuery, pagerQuery);

                //Assert
                Assert.AreEqual(1, result.Count);
            }
        }

        [TestMethod]
        public async Task CustomerGetFilterByPhoneCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("CustomerGetFilterByPhoneCorrectly");
            var customer = Utils.GetUserRoles().Where(u => u.RoleId == 2).FirstOrDefault();
            PagerQueryObject pagerQuery = new PagerQueryObject(1, 1);
            FilterUserQuery userQuery = new FilterUserQuery();

            userQuery.Phone = "0888888888";

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new UserService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllCustomersAsync(userQuery, pagerQuery);

                //Assert
                Assert.AreEqual(1, result.Count);
            }
        }

        [TestMethod]
        public async Task CustomerGetFilterByVehicleCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("CustomerGetFilterByVehicleCorrectly");
            var customer = Utils.GetUserRoles().Where(u => u.RoleId == 2).FirstOrDefault();
            var vehicle = Utils.GetVehicles().Where(v => v.CustomerId == customer.UserId);
            PagerQueryObject pagerQuery = new PagerQueryObject(1, 1);
            FilterUserQuery userQuery = new FilterUserQuery();

            userQuery.Vehicle = "Ka";

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new UserService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllCustomersAsync(userQuery, pagerQuery);

                //Assert
                Assert.AreEqual(1, result.Count);
            }
        }

        [TestMethod]
        public async Task CustomerGetFilterByDateCorrectly()
        {
            var modelMapperFake = new Mock<IModelMapper>();
            var options = Utils.GetOptions("CustomerGetFilterByDateCorrectly");
            PagerQueryObject pagerQuery = new PagerQueryObject(1, 1);
            FilterUserQuery userQuery = new FilterUserQuery();

            userQuery.From = DateTime.Now.AddDays(-2);
            userQuery.To = DateTime.Now.AddDays(2);

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new UserService(actContext, modelMapperFake.Object);
                var result = await sut.GetAllCustomersAsync(userQuery, pagerQuery);

                //Assert
                Assert.AreEqual(1, result.Count);
            }
        }
    }
}
