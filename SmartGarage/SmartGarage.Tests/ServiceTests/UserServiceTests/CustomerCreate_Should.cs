﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models.CreateDTOs;
using SmartGarage.Services.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.UserServiceTests
{
    [TestClass]
    public class CustomerCreate_Should
    {
        [TestMethod]
        public async Task CustomerCreateCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("CustomerCreateCorrectly");
            var usersCount = Utils.GetUsers().Count();
            var model = new CreateUserDTO()
            {
                Email = "testfakeemail@gmail.com",
                FirstName = "Place",
                LastName = "Holder",
                Phone = "0999999999"
            };

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new UserService(actContext, modelMapper);
                var result = await sut.CreateCustomerAsync(model);

                //Assert
                Assert.IsTrue(result);
                Assert.AreEqual(usersCount + 1, actContext.Users.Count());

            }
        }
    }
}
