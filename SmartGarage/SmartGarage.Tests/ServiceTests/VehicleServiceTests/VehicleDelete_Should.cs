﻿using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SmartGarage.Data;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Tests.ServiceTests.VehicleServiceTests
{
    [TestClass]
    public class VehicleDelete_Should
    {
        [TestMethod]
        public async Task VehicleDeleteCorrectly()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("VehicleDeleteCorrectly");
            var vehicleModelToDelete = Utils.GetVehicles().FirstOrDefault();

            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VehicleService(actContext, modelMapper);
                var result = await sut.DeleteAsync(vehicleModelToDelete.Id);

                //Assert
                Assert.IsTrue(result);
                Assert.IsTrue(actContext.Vehicles.IgnoreQueryFilters().FirstOrDefault(x => x.Id == vehicleModelToDelete.Id).IsDeleted);

            }

        }
        [TestMethod]
        public async Task VehicleDeleteWrongID()
        {
            var modelMapper = new ModelMapper();
            var options = Utils.GetOptions("VehicleDeleteWrongID");



            using (var arrangeContext = new SmartGarageContext(options))
            {
                Utils.FillBase(arrangeContext);
                await arrangeContext.SaveChangesAsync();
            }
            //Act
            using (var actContext = new SmartGarageContext(options))
            {
                var sut = new VehicleService(actContext, modelMapper);
                var result = await sut.DeleteAsync(0);

                //Assert
                Assert.IsFalse(result);

            }

        }
    }
}
