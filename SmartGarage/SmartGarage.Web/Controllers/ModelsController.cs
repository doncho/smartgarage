﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SmartGarage.Data.Models;
using SmartGarage.Services.Models;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using SmartGarage.Web.Models;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.Controllers
{
    [Authorize()]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("[controller]")]
    public class ModelsController : Controller
    {
        private readonly IVehicleModelService vehicleModelService;
        private readonly IManufacturerService manufacturerService;
        private readonly IVehicleService vehicleService;
        private readonly UserManager<User> userManager;
        public ModelsController(IVehicleModelService vehicleModelService,
                                        IManufacturerService manufacturerService,
                                        UserManager<User> userManager,
                                        IVehicleService vehicleService)
        {
            this.vehicleModelService = vehicleModelService;
            this.manufacturerService = manufacturerService;
            this.userManager = userManager;
            this.vehicleService = vehicleService;
        }

        [Authorize(Roles = "Employee")]
        [HttpGet]
        public async Task<IActionResult> Index([FromQuery] int id, int pageNumber = 1)
        {
            var pagerQueryObj = new PagerQueryObject(pageNumber, 0);

            var input = await manufacturerService.GetAync(id);

            var models = await vehicleModelService.GetAllAsync(input.Name, pagerQueryObj);

            return View(models);
        }

        [Authorize(Roles = "Employee")]
        [HttpGet("Create/{id}")]
        public async Task<IActionResult> Create(int id)
        {
            var manufacturer = await manufacturerService.GetAync(id);

            var model = new ModelsViewModel()
            {
                ManufacturerId = manufacturer.Id,
                Manufacturer = manufacturer.Name
            };

            return View(model);
        }

        [Authorize(Roles = "Employee")]
        [HttpPost("Create/{id}")]
        public async Task<IActionResult> Create(ModelsViewModel model, int id)
        {
            if (model == null)
            {
                model = new ModelsViewModel();
            };
            var modelToCreate = new VehicleModelDTO
            {
                ManufacturerId = id,
                Manufacturer = model.Manufacturer,
                Name = model.Name
            };
            if (ModelState.IsValid)
            {
                await vehicleModelService.CreateAsync(modelToCreate);
                TempData["Messages"] = "Successfully created new vehicle model";
                return RedirectToAction("Index", new { id = id });
            }
            return View(modelToCreate);
        }


        [Authorize(Roles = "Employee")]
        [HttpGet("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var vehicleModel = await vehicleModelService.GetAsync(id);
            var model = new ModelsViewModel
            {
                Name = vehicleModel.Name,
                Manufacturer = vehicleModel.Manufacturer
            };

            if (vehicleModel == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost("Delete/{id}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var vehicleToDelete = await vehicleModelService.GetAsync(id);
            var vehicleToDeleteManufacturerId = vehicleToDelete.ManufacturerId;

            var result = await vehicleModelService.DeleteAsync(id);
            if (result)
            {
                TempData["Messages"] = "Successfully deleted";
            }
            return RedirectToAction("Index", new { id = vehicleToDeleteManufacturerId });
        }

        [Authorize(Roles = "Employee")]
        [HttpGet("CreatePoPModel")]
        public async Task<IActionResult> CreatePoPModel(string name, string manName)
        {
            int manId;
            var incoming = int.TryParse(manName,out manId);
            if (name == null)
            {
                return PartialView("_MessagePartial", new MessagePartial { Message = "You need to enter valid name", GoodOrBad = false });
            }
            if (!incoming)
            {

                var mans = await manufacturerService.GetAllAync();
                var id = mans.FirstOrDefault(x => x.Name == manName).Id;

                var modelToCreate = new VehicleModelDTO
                {
                    ManufacturerId = id,
                    Name = name
                };
                if (vehicleModelService.GetAllAsync().Result.Where(x => x.ManufacturerId == id).Select(x => x.Name).Contains(name))
                {
                    return PartialView("_MessagePartial", new MessagePartial { Message = "Model already exists for this manufacturer", GoodOrBad = false });
                }

                await vehicleModelService.CreateAsync(modelToCreate);
                return PartialView("_MessagePartial", new MessagePartial { Message = "Successfully created new vehicle model", GoodOrBad = true });
            }
            else
            {
            
                var modelToCreate = new VehicleModelDTO
                {
                    ManufacturerId = manId,
                    Name = name
                };
                var res = await vehicleModelService.GetAllAsync();
                if (res.Where(x => x.ManufacturerId == manId).Select(x => x.Name).Contains(name))
                {
                    return PartialView("_MessagePartial", new MessagePartial { Message = "Model already exists for this manufacturer", GoodOrBad = false });
                }

                await vehicleModelService.CreateAsync(modelToCreate);
                return PartialView("_MessagePartial", new MessagePartial { Message = "Successfully created new vehicle model", GoodOrBad = true });

            }


        }
    }
}

