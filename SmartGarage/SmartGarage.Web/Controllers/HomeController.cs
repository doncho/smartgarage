﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SmartGarage.Web.Models;
using System.Diagnostics;
using System.Threading.Tasks;

namespace SmartGarage.Web.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
        public IActionResult PageNotFound()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        
        public IActionResult Location()
        {
            return View();
        }
        public  IActionResult ContactUs()
        {
            return View();
        }

        [HttpPost("Redirect")]
        public IActionResult ContactUsRedirect()
        {
            TempData["Messages"] = "Thank you for contacting SmartGarage, we will be in contact with you shortly";
            return RedirectToAction(nameof(ContactUs));
        }
    }
}
