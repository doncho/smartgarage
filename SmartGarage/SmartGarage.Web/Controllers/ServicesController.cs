﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SmartGarage.Data.Models;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using System.Threading.Tasks;

namespace SmartGarage.Web.Controllers
{
    [Authorize()]
    [Route("[controller]")]
    public class ServicesController : Controller
    {
        private readonly ISvcService svcService;
        private readonly IModelMapper modelMapper;
        private readonly UserManager<User> userManager;
        public ServicesController(ISvcService svcService, IModelMapper modelMapper, UserManager<User> userManager)
        {
            this.svcService = svcService;
            this.modelMapper = modelMapper;
            this.userManager = userManager;
        }

        [Authorize()]
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var pagerQueryObj = new PagerQueryObject(1, 0);

            var serviceQuery = new FilterServiceQuery();
            return View(await svcService.GetAllAsync(serviceQuery, pagerQueryObj));
        }

        [Authorize(Roles = "Employee")]
        [HttpGet("Create")]
        public async Task<IActionResult> Create()
        {
            ViewData["Id"] = new SelectList(await this.svcService.GetAllAsync(), "Name", "Price", "Description");
            return View();
        }

        [Authorize(Roles = "Employee")]
        [HttpPost("Create")]
        public async Task<IActionResult> Create([Bind("Name, Description, Price")] ServiceDTO service)
        {
            if (ModelState.IsValid)
            {
                await svcService.CreateAsync(service);
                TempData["Messages"] = "Successfully created new service";
                return RedirectToAction("Index", "Services");
            }
            return View(service);
        }

        [Authorize()]
        [HttpGet("Details")]
        public async Task<IActionResult> Details(int id)
        {
            var visit = await svcService.GetAsync(id);

            if (visit == null)
            {
                return NotFound();
            }

            return View(visit);
        }

        [Authorize(Roles = "Employee")]
        [HttpGet("Edit/{id}")]
        public async Task<IActionResult> Edit(int id)
        {
            var service = await svcService.GetAsync(id);
            var model = new ServiceDTO
            {
                Name = service.Name,
                Price = service.Price,
                Description = service.Description
            };

            return View(model);
        }

        [Authorize(Roles = "Employee")]
        [HttpPost("Edit/{id}")]
        public async Task<IActionResult> Edit(int id, ServiceDTO model)
        {
            var serviceToUpdate = new ServiceDTO()
            {
                Name = model.Name,
                Price = model.Price,
                Description = model.Description
            };

            var result = await svcService.UpdateAsync(id, serviceToUpdate);
            if (result)
            {
                TempData["Messages"] = "Successfully updated";
            }

            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "Employee")]
        [HttpGet("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var service = await svcService.GetAsync(id);

            var model = new ServiceDTO
            {
                Name = service.Name,
                Price = service.Price
            };

            if (service == null)
            {
                return NotFound();
            }

            return View(model);
        }

        [HttpPost("Delete/{id}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var result = await svcService.DeleteAsync(id);

            if (result)
            {
                TempData["Messages"] = "Successfully deleted";
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpGet("ServicePager")]
        public async Task<IActionResult> ServicePager(string name, decimal priceFrom, decimal priceTo, int pageNum)
        {
            var pagerQueryObj = new PagerQueryObject(pageNum, 0);

            var serviceQuery = new FilterServiceQuery()
            {
                Name = name,
                PriceFrom = priceFrom,
                PriceTo = priceTo
            };

            return PartialView("_PartialViewServicesPager", await svcService.GetAllAsync(serviceQuery, pagerQueryObj));
        }
    }
}
