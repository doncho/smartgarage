﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using SmartGarage.Data.Models;
using SmartGarage.Services.Models;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using SmartGarage.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.Controllers
{
    [Authorize()]
    [Route("[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ManufacturersController : Controller
    {
        private readonly IVehicleModelService vehicleModelService;
        private readonly IManufacturerService manufacturerService;
        private readonly IVehicleService vehicleService;
        private readonly UserManager<User> userManager;
        public ManufacturersController(IManufacturerService manufacturerService, UserManager<User> userManager, IVehicleModelService vehicleModelService, IVehicleService vehicleService)
        {
            this.manufacturerService = manufacturerService;
            this.userManager = userManager;
            this.vehicleModelService = vehicleModelService;
            this.vehicleService = vehicleService;
        }
        [Authorize(Roles = "Employee")]
        [HttpGet]
        public async Task<IActionResult> Index(int pageNumber = 1)
        {
            var pagerQueryObj = new PagerQueryObject(pageNumber, 0);

            return View(await manufacturerService.GetAllAync(pagerQueryObj));
        }
        [Authorize(Roles = "Employee")]
        [HttpGet("Restore")]
        public async Task<IActionResult> IndexDeleted(int pageNumber = 1)
        {
            var pagerQueryObj = new PagerQueryObject(pageNumber, 0);

            return View(await manufacturerService.GetAllDeletedAync(pagerQueryObj));
        }

       
        [Authorize(Roles = "Employee")]
        [HttpGet("CreatePopUp")]
        public async Task<IActionResult> CreatePopUp(string name)
        {
            var model = new ManufacturerDTO()
            {
                Name = name
            };

            if (name != null)
            {
                try
                {
                    await manufacturerService.CreateAync(model);
                    //TempData["Messages"] = "Successfully created new vehicle manufacturer";                
                    return PartialView("_MessagePartial", new MessagePartial { Message = "Successfully created new vehicle manufacturer", GoodOrBad = true });
                }
                catch
                {

                    return PartialView("_MessagePartial", new MessagePartial { Message = "Manufacturer already exists", GoodOrBad = false });
                }    
                
            }
            return PartialView("_MessagePartial", new MessagePartial { Message = "You need to enter valid name", GoodOrBad = false });
        }


        [Authorize(Roles = "Employee")]
        [HttpGet("Create")]
        public async Task<IActionResult> Create()
        {
            ViewData["Id"] = new SelectList(await this.manufacturerService.GetAllAync(), "Name");
            return View();
        }

        [Authorize(Roles = "Employee")]
        [HttpPost("Create")]
        public async Task<IActionResult> Create([Bind("Name")] ManufacturerDTO model)
        {
            if (ModelState.IsValid)
            {
                await manufacturerService.CreateAync(model);
                TempData["Messages"] = "Successfully created new vehicle manufacturer";
                return RedirectToAction("Index", "Manufacturers");
            }
            return View(model);
        }

        [Authorize(Roles = "Employee")]
        [HttpGet("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var manufacturer = await manufacturerService.GetAync(id);
            var model = new ManufacturerDTO
            {
                Name = manufacturer.Name,
            };
            if (manufacturer == null)
            {
                return NotFound();
            }
            return View(model);
        }

        [Authorize(Roles = "Employee")]
        [HttpPost("Delete/{id}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var result = await manufacturerService.DeleteAsync(id);
            if (result)
            {
                TempData["Messages"] = "Successfully deleted";
            }
            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "Employee")]
        [HttpGet("Restore/{id}")]
        public async Task<IActionResult> Restore(int id)
        {
            var manufacturer = await manufacturerService.GetAync(id);
            var model = new ManufacturerDTO
            {
                Name = manufacturer.Name,
            };
            if (manufacturer == null)
            {
                return NotFound();
            }
            return View(model);
        }

        [Authorize(Roles = "Employee")]
        [HttpPost("Restore/{id}"), ActionName("Restore")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RestoreConfirmed(int id)
        {
            var result = await manufacturerService.RestoreAsync(id);
            if (result)
            {
                TempData["Messages"] = "Successfully restored";
            }
            return RedirectToAction(nameof(Index));
        }
    }
}
