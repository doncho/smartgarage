﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.WebUtilities;
using SmartGarage.Data.Models;
using SmartGarage.Services.Helpers;
using SmartGarage.Services.ModelMapper;
using SmartGarage.Services.Models;
using SmartGarage.Services.Models.CreateDTOs;
using SmartGarage.Services.Models.UpdateDTOs;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using SmartGarage.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace SmartGarage.Web.Controllers
{
    [Authorize()]
    [Route("[controller]")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class CustomersController : Controller
    {
        private readonly IVehicleService vehicleService;
        private readonly IManufacturerService manufacturerService;
        private readonly IVehicleModelService vehicleModelService;
        private readonly IUserService userService;
        private readonly IModelMapper modelMapper;
        private readonly IVisitService visitService;
        private readonly UserManager<User> userManager;

        public CustomersController(IVehicleService vehicleService, IManufacturerService manufacturerService,
            IVehicleModelService vehicleModelService, IModelMapper modelMapper,
            IUserService userService, IVisitService visitService, UserManager<User> userManager)
        {
            this.vehicleService = vehicleService;
            this.manufacturerService = manufacturerService;
            this.vehicleModelService = vehicleModelService;
            this.modelMapper = modelMapper;
            this.userService = userService;
            this.visitService = visitService;
            this.userManager = userManager;
        }
        public async Task<IActionResult> Index()
        {
            var pagerQueryObj = new PagerQueryObject(1, 0);

            var filterCustomers = new FilterUserQuery();

            var customers = await userService.GetAllCustomersAsync(filterCustomers, pagerQueryObj);

            return View(customers);
        }

        [Authorize(Roles = "Employee")]
        [HttpGet("Details")]
        public async Task<IActionResult> Details(int id)
        {
            var customer = await userService.GetCustomerAsync(id);
            if (customer == null)
            {
                return NotFound();
            }

            return View(customer);
        }
        [Authorize(Roles = "Employee")]
        [HttpGet("Delete/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var user = await userService.GetCustomerAsync(id);

            var model = new CustomerViewModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email
            };

            if (user == null)
            {
                return NotFound();
            }

            return View(model);
        }
        [Authorize(Roles = "Employee")]
        [HttpPost("Delete/{id}"), ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var result = await userService.DeleteAsync(id);

            if (result)
            {
                TempData["Messages"] = "Successfully deleted";
            }

            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = "Employee")]
        [HttpGet("Edit/{id}")]
        public async Task<IActionResult> Edit(int id)
        {
            var user = await userService.GetCustomerAsync(id);
            var model = new CustomerViewModel
            {
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Phone = user.Phone,
            };

            return View(model);
        }

        [Authorize(Roles = "Employee")]
        [HttpPost("Edit/{id}")]
        public async Task<IActionResult> Edit([Bind("FirstName,LastName,Email,Phone")] CustomerViewModel model, int id)
        {
            var userToUpdate = new UpdateUserDTO()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                Phone = model.Phone,
            };

            var result = await userService.UpdateAsync(userToUpdate, id);
            if (result)
            {
                TempData["Messages"] = "Successfully updated";
            }

            return RedirectToAction(nameof(Index));
        }

        [HttpGet("PartialPager")]
        public async Task<IActionResult> PartialPager(string customerName, string email, string phone, string vehicle,
            DateTime from, DateTime to,string order,bool dateFilter,string filterBy, int pageNumber = 1)
        {

            var pagerQueryObj = new PagerQueryObject(pageNumber, 0);

            var filterCustomers = new FilterUserQuery()
            {
                Name = customerName,
                Email = email,
                Phone = phone,
                Vehicle = vehicle,
                From = from,
                To = to,
                Order = order,
                DateFilter = dateFilter,
                SortBy = filterBy
                
            };

            var customers = await userService.GetAllCustomersAsync(filterCustomers, pagerQueryObj);

            return PartialView("_CustomersPartialViewPager",customers);
        }

        [HttpGet("ChangePassword/{id}")]
        public async Task<IActionResult> ChangePassword (int id)
        {
            var customer = await userService.GetUserAsync(id);

            return View(customer);
        }
        [HttpPost("ChangePassword/{id}")]
        public async Task<IActionResult> ChangePassword(int id,string password)
        {
            var result = await userService.ResetPassword(password,id);
            if (result)
            {
                TempData["Messages"] = "Successfully changed password";
            }
            else
            {
                TempData["Messages"] = "Invalid password";
            }
            var customer = await userService.GetUserAsync(id);
            return View(customer);
        }

        [HttpPost("SendResetPassowrd")]
        public async Task<IActionResult> SendResetPassowrd(string email)
        {
            var user = await userManager.FindByEmailAsync(email);
            if (email!=null)
            {
                var code = await userManager.GeneratePasswordResetTokenAsync(user);
                code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                var callbackUrl = Url.Page(
                    "/Account/ResetPassword",
                    pageHandler: null,
                    values: new { area = "Identity", code },
                    protocol: Request.Scheme);

                SendEmail.SendPassword(
                    $"Please reset your password by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.",email);
                TempData["Messages"] = "Successfully send link to email";
               return RedirectToAction("Details", new { id = user.Id });
            }
            TempData["Messages"] = "Something went wrong";
            return RedirectToAction("Details", new { id = user.Id });
        }

        [HttpGet("SendPdf/{id}")]
        public async Task<IActionResult> SendPdf(int id)
        {
            var visits = await visitService.GetAllAsync(id);
            return View(visits.ToList());
            
        }

        [HttpPost("SendPdf/{id}")]
        public async Task<IActionResult> SendPdf(List<VisitDTO> comingVisits,int id)
        {
            var customer = await userService.GetCustomerAsync(id);
            var visits = await visitService.GetAllAsync(id);
            var visitsToSend = comingVisits.Where(x => x.ToSend).Select(x => x.Id);
            await SendEmail.SendPdfAsync(visits.Where(x=> visitsToSend.Contains(x.Id)).ToList(),customer.Email);

            TempData["Messages"] = "Email sended";
            return RedirectToAction("Details", new { id = customer.Id });

        }
    }
}
