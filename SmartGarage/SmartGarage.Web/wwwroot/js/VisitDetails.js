﻿ 
    $(document).ready(function () {
            $("#currency").on("change", function () {
                var val = $('#currency').val();
                $.ajax({
                    url: "/Visit/ExchangeCurrency",
                    type: "GET",
                    data: { currencyChange: val, id: $('#visitId').val() }
                })
                    .done(function (partialViewResult) {
                        $("#currencyExchange").html(partialViewResult);
                    });

            });
        });

        const downloadPdf = () => {
            var element = document.getElementById('pdfContainer');
            var opt = {
                margin: 0.2,
                filename: 'myfiles.pdf',
                image: { type: 'jpeg', quality: 0.98 },
                html2canvas: { scale: 1.2 },
                jsPDF: { unit: 'in', format: 'A4', orientation: 'portrait' },
            };
            html2pdf().from(element).set(opt).save();
        }