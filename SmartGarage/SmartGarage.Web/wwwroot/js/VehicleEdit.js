﻿$(document).ready(function () {
    var MakeDDL = $("#Make");
    var ModelDDL = $("#Model");

    MakeDDL.change(function () {
        if ($(this).val() == "0") {
            ModelDDL.prop('disabled', true);
            ModelDDL.val("0");
        }
        else {
            $.ajax({
                url: '/Vehicles/Manufacturermodels/' + $(this).val(),
                method: "get",
                headers: {
                    'RequestVerificationToken': '@AntiForgery.GetAndStoreTokens(Context).RequestToken'
                },
                success: function (data) {
                    ModelDDL.prop('disabled', false);
                    ModelDDL.empty();
                    ModelDDL.append($('<option/>', { value: '0', text: '--Select Model--' }));
                    $(data).each(function (index, item) {
                        ModelDDL.append($('<option/>', { value: item.id, text: item.name }));
                    });
                }
            });
        }
    });

});