﻿using SmartGarage.Services.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Web.Models
{
    public class CreateVisitNotRegisterdCustomer
    {
        //Customer

        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [MinLength(2), MaxLength(20)]
        public string FirstName { get; set; }

        [Required]
        [MinLength(2), MaxLength(20)]
        public string LastName { get; set; }

        [Required]
        [RegularExpression(@"[0]{1}\d{9}", ErrorMessage = "Invalid phone number")]
        public string Phone { get; set; }

        //Vehicle
        [Required]
        [Display(Name = "Plate Number")]
        [RegularExpression(@"[A-Z]{1,2}\d{4}[A-Z]{2}", ErrorMessage = "Invalid plate number")]
        public string PlateNumber { get; set; }
        [Required]
        [Display(Name = "Identity Number")]
        public string IdentityNumber { get; set; }
        [Required]
        [Range(0, 15, ErrorMessage = "Types are between 0 and 15")]
        public int Type { get; set; }
        public IEnumerable<ManufacturerDTO> Manufacturers { get; set; }
        public IEnumerable<VehicleModelDTO> Models { get; set; }

        [Required]
        [Display(Name = "Manufacturer")]
        public string ManufacturerId { get; set; }

        [Required]
        [Display(Name = "Model")]
        public string VehicleModelId { get; set; }

        //visit
        public List<ServiceDTO> Services { get; set; }
    }
}
