﻿using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Web.Models
{
    public class ServiceViewModel
    {
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Price")]
        public decimal Price { get; set; }
    }
}
