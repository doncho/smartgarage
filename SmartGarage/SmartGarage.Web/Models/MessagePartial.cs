﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.Models
{
    public class MessagePartial
    {
        public string Message { get; set; }
        public bool GoodOrBad { get; set; }
    }
}
