﻿using SmartGarage.Services.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Web.Models
{
    public class VehicleViewModel
    {
        [Display(Name = "Customer")]
        public string CustomerId { get; set; }
        [Required]
        [Display(Name = "Plate Number")]
        [RegularExpression(@"[A-Z]{1,2}\d{4}[A-Z]{2}", ErrorMessage = "Invalid plate number")]
        public string PlateNumber { get; set; }
        [Required]
        [Display(Name = "Identity Number")]
        [MinLength(17), MaxLength(17)]
        public string IdentityNumber { get; set; }
        [Required]
        [Range(0, 15, ErrorMessage = "Types are between 0 and 15")]
        public string Type { get; set; }
        public IEnumerable<ManufacturerDTO> Manufacturers { get; set; }
        public IEnumerable<VehicleModelDTO> Models { get; set; }
        public IEnumerable<UserDTO> Customers { get; set; }
        public string CustomerName { get; set; }
        //TODO: custem attrr needed

        [Display(Name = "Manufacturer")]
        public string ManufacturerId { get; set; }

        [Display(Name = "Model")]
        public string VehicleModelId { get; set; }

    }
}
