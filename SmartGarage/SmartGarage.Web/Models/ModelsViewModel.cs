﻿using System.ComponentModel.DataAnnotations;

namespace SmartGarage.Web.Models
{
    public class ModelsViewModel
    {
        public int Id { get; set;}
        public string Name { get; set; }
        public int ManufacturerId { get; set; }

        public string Manufacturer { get; set; }
    }
}
