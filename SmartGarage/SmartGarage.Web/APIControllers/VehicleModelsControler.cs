﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Models;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace SmartGarage.Web.APIControllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Authorize(Roles = "Employee")]
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleModelsController : Controller
    {
        private readonly IVehicleModelService modelService;
        public VehicleModelsController(IVehicleModelService modelService)
        {
            this.modelService = modelService;
        }
        [Authorize(Roles = "Employee")]
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery]string manufacturer, int pageNumber,int pageSize)
        {
            var pager = new PagerQueryObject(pageNumber,pageSize);
            var result = await modelService.GetAllAsync(manufacturer, pager);
            if (result.Count != 0)
            {
                return Ok(result);
            }
            return NoContent();
        }
        [Authorize(Roles = "Employee")]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var result = await modelService.GetAsync(id);
            if (result != null)
            {
                return Ok(result);
            }
            return NotFound();
        }
        [Authorize(Roles = "Employee")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] VehicleModelDTO mdodel)
        {
            var result = await modelService.CreateAsync(mdodel);
            if (result)
            {
                return Ok();
            }
            return BadRequest();
        }
        [Authorize(Roles = "Employee")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] VehicleModelDTO model)
        {
            var result = await modelService.UpdateAsync(model, id);
            if (result)
            {
                return Ok();
            }
            return BadRequest();
        }
        [Authorize(Roles = "Employee")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await modelService.DeleteAsync(id);
            if (result)
            {
                return Ok();
            }
            return BadRequest();
        }
        [Authorize(Roles = "Employee")]
        [HttpGet("manufacturer/{manufacturer}")]
        public IActionResult GetModelsByManufacturer(int manufacturer)
        {
            return Ok(modelService.GetAllAsync().Result.Where(x => x.ManufacturerId == manufacturer));
        }
    }
}
