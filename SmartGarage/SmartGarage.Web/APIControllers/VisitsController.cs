﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.CustomExceptions;
using SmartGarage.CustomExceptions.CustomExceptions;
using SmartGarage.Services.Models.CreateDTOs;
using SmartGarage.Services.Models.UpdateDTOs;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SmartGarage.Web.APIControllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class VisitsController : ControllerBase
    {
        private readonly IVisitService visitService;
        public VisitsController(IVisitService visitService)
        {
            this.visitService = visitService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllAsync([FromQuery] FilterVisitQuery visitQuery, [FromQuery] int pageNumber, int pageSize, string currency)
        {
            var pagerQuery = new PagerQueryObject(pageNumber, pageSize);
            try
            {
                return Ok(await visitService.GetAllAsync(visitQuery, pagerQuery, currency));
            }
            catch (InvalidVisit e)
            {
                return BadRequest(e.Message);
            }

        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id, [FromQuery] string currency)
        {
            var visit = await visitService.GetAsync(id, currency);

            if (visit == null)
            {
                return NoContent();
            }
            return Ok(visit);
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] CreateVisitDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }
            return Ok(await visitService.CreateAsync(model));
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] UpdateVisitDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }

            return Ok(await visitService.UpdateAsync(id, model));
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id >= 1)
            {
                var result = await this.visitService.DeleteAsync(id);

                if (result)
                {
                    return Ok();
                }
            }

            return BadRequest("Failed to delete visit due to invalid ID.");
        }

        [HttpPut("addservice/{id}")]
        public async Task<IActionResult> AddService(int id, [FromQuery] int serviceId)
        {
            if (serviceId == default)
            {
                return BadRequest();
            }

            return Ok(await visitService.AddServiceToVisit(id, serviceId));
        }

        [HttpPut("checkout/{id}")]
        public async Task<IActionResult> Checkout(int id)
        {
            try
            {
                return Ok(await visitService.CheckOutVisit(id));
            }
            catch (InvalidVisitStatus e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
