﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SmartGarage.Services.Models;
using SmartGarage.Services.QueryObject;
using SmartGarage.Services.Services.Interfaces;
using System.Net;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SmartGarage.Web.APIControllers
{
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [Route("api/[controller]")]
    [ApiController]
    public class ServicesController : ControllerBase
    {
        private readonly ISvcService svcService;
        public ServicesController(ISvcService svcService)
        {
            this.svcService = svcService;
        }
        [Authorize()]
        [HttpGet]
        public async Task<IActionResult> Get([FromQuery] FilterServiceQuery serviceQuery, int pageNumber, int pageSize)
        {
            var pagerQuery = new PagerQueryObject(pageNumber, pageSize);
            return Ok(await svcService.GetAllAsync(serviceQuery, pagerQuery));
        }
        [Authorize(Roles = "Employee")]
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var service = await svcService.GetAsync(id);
            if (service == null)
            {
                return NoContent();
            }
            return Ok(service);
        }
        [Authorize(Roles = "Employee")]
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] ServiceDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }
            return Ok(await svcService.CreateAsync(model));
        }
        [Authorize(Roles = "Employee")]
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] ServiceDTO model)
        {
            if (model == null)
            {
                return BadRequest();
            }
            return Ok(await svcService.UpdateAsync(id, model));
        }
        [Authorize(Roles = "Employee")]
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await svcService.DeleteAsync(id);

            if (result)
            {
                return Ok();
            }
            else
            {
                return NotFound();
            }
        }
    }
}
